import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateticketComponent } from './createticket/createticket.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';

const routes: Routes = [
  { path: "", component: TicketListComponent }, 
{ path: "tickets/Create", component: CreateticketComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketsRoutingModule { }
