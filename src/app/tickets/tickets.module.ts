import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketsRoutingModule } from './tickets-routing.module';
import { CreateticketComponent } from './createticket/createticket.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 

@NgModule({
  declarations: [
    CreateticketComponent,
    TicketListComponent
  ],
  imports: [
    CommonModule,
    TicketsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TicketsModule { }
