import { Component, Inject, Injectable, Input, OnInit } from '@angular/core';
import { TicketsServiceService } from '../tickets-service.service';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})

export class TicketListComponent implements OnInit {
  public name: string = "";
  public tickets: any[] | undefined;


  constructor(public ticketService: TicketsServiceService) {
  }

  ngOnInit(): void {
    this.getlist();
  }


  getlist() {
    let lk = this.ticketService.getticketlist();
    this.ticketService.getticketlist().subscribe((data: any[]) => {
      this.tickets = data;
    });
  }
}
