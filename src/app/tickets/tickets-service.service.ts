import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class TicketsServiceService {

  apiUrl = 'https://localhost:44352';
  constructor(public http: HttpClient) {
  }


  getticketlist(): any {
    console.log("ticket method")
    let result = this.http.get<any[]>(`${this.apiUrl}/api/Ticket/GetTicketList`);
    return result;
  }


}
